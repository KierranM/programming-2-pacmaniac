﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PacManiac
{
    public enum Direction :byte //This is a new Enumeration that gives the four possible directions an easier representation than 0,1,2...
    {
        Down = 0,
        Right = 1,
        Up = 2,
        Left = 3,
    }
}
