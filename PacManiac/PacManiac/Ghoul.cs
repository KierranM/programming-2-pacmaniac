﻿/**************************Class Description***************************
 * Class Name: Ghoul                                                  *
 *                                                                    *
 * Description: The Ghoul class derives from Creature and defines     *
 *              the characteristics of the antagonist of the game     *
 *              the ghouls. The ghoul is similar to PacMan with the   *
 *              exception of his movement, the ghoul moves itself     *
 *              along a path, at each step it checks if there are     *
 *              alternatives directions to travel and chooses a new   *
 *              path semi-randomly although it will never head back   *
 *              the way it came and the current direction is          *
 *              preferred.                                            *
 *                                                                    *
 **********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Media;
using System.Drawing;
using System.Windows.Forms;

namespace PacManiac
{
    public class Ghoul :Creature
    {
        //Constants
        const string IMAGEPATH = @"Resources\Images\";
        const string BMPIMAGE = ".bmp";
        const string IMAGELEFT = "-left";
        const string IMAGEUP = "-up";
        const string IMAGEDOWN = "-down";

        const int DIRECTIONINCREASE = 2;
        const int NUMBEROFDIRECTIONS = 4;
        const int SAMEDIRECTIONBOOST = 30;
        const int CHASEPACMANBOOST = 25;
        const int MAXIMUMRANDOM = 101;
        const int OTHERGHOULS = 3;

        //Fields
        private Bitmap leftImage;
        private Bitmap upImage;
        private Bitmap downImage;
        private Ghoul[] otherGhouls = new Ghoul[OTHERGHOULS];

        public Ghoul[] OtherGhouls
        {
            get { return otherGhouls; }
            set { otherGhouls = value; }
        }
    
        public Ghoul(string name, Point startingLocation, Maze maze) //This is the constructor for Ghoul as it has no fields of its own, the Ghouls constructor simple calls the base class constructor.
            :base(name, startingLocation, maze)
        {
            try
            {
                this.leftImage = new Bitmap(IMAGEPATH + name + IMAGELEFT + BMPIMAGE);
                this.upImage = new Bitmap(IMAGEPATH + name + IMAGEUP + BMPIMAGE);
                this.downImage = new Bitmap(IMAGEPATH + name + IMAGEDOWN + BMPIMAGE);
            }
            catch
            {
                MessageBox.Show("A fatal error has occured. Please reinstall the game", "Fatal Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }
        }

        public override void Draw() //This is Ghouls implementation of the base classes abstract Draw() method. It places the ghouls image into the cell corresponding to its current location.
        {
            DataGridViewCell cell = GetCellFromPosition(Location);

            cell.Value = GetImageToMatchDirection();
        }

        private Bitmap GetImageToMatchDirection()//Returns a bitmap image of the ghoul to match its current direction
        {
            Bitmap returnImage = null;

            switch (CurrentDirection)
            {
                case Direction.Down:
                    returnImage = downImage;
                    break;
                case Direction.Right:
                    returnImage = Image;
                    break;
                case Direction.Up:
                    returnImage = upImage;
                    break;
                case Direction.Left:
                    returnImage = leftImage;
                    break;
            }

            return returnImage;
        }

        public bool CheckCanMove(PacMan pac, Direction directionToCheck) //This overloaded CheckCanMove is designed specifically for the Ghoul class and checks the same conditions as the base class and also if the next position contains another ghoul
        {
            bool canMove = false;

            Point nextLocation = GetNextLocation(directionToCheck);
            DataGridViewCell nextCell = GetCellFromPosition(nextLocation);

            if (nextCell.Value != Maze.Wall)
            {
                canMove = true;
            }

            foreach (Ghoul ghoul in otherGhouls)
            {
                if (ghoul.Location == nextLocation)
                {
                    canMove = false;
                }
            }

            return canMove;
        }

        private Direction DirectionChange(bool[] availableDirections, Random r, PacMan pac)
        //This gives each possible direction a random chance of being chosen, unless that direction is opposite to the current direction. 
        //Current direction is given precedence by adding 30 to its chance of being chosen, and the direction which the ghould would need 
        //to travel to reach pacman is also boosted by 25. The highest chance is the new Direction.
        {
            int[] directionsChance = new int[NUMBEROFDIRECTIONS];

            for (int i = 0; i < NUMBEROFDIRECTIONS; i++)
            {
                    if (availableDirections[i] == true)
                    {
                        directionsChance[i] = r.Next(MAXIMUMRANDOM);
                        if (i == (int)CurrentDirection)
                        {
                            directionsChance[i] += SAMEDIRECTIONBOOST;
                        }
                        if ((Direction)i == pac.GetDirectionFromPointToPacMan(Location))
                        {
                            directionsChance[i] += CHASEPACMANBOOST;
                        }
                        if ((Direction)i == (Direction)(((int)CurrentDirection + DIRECTIONINCREASE) % NUMBEROFDIRECTIONS))
                        {
                            directionsChance[i] = 0;
                        }
                    }
            }

            int highestChance = 0;
            Direction newDirection = Direction.Down;

            for (int i = 0; i < NUMBEROFDIRECTIONS; i++)
            {
                if (directionsChance[i] >= highestChance)
                {
                    highestChance = directionsChance[i];
                    newDirection = (Direction)i;
                }
            }

            return newDirection;
        }

        public void PathFinder(PacMan pac, Random r) //Checks each possible direction and stores if they are valid in a Boolean array which is passed to DirectionChange and used to calculate the new Direction. This method then assigns the new direction to the CurrentDirection property.
        {
            bool[] possibleDirections = new bool[NUMBEROFDIRECTIONS];

            for (int i = 0; i < NUMBEROFDIRECTIONS; i++)
            {
                possibleDirections[i] = CheckCanMove(pac, (Direction)i);
            }

            CurrentDirection = DirectionChange(possibleDirections, r, pac);
        }

        public bool CheckForPacMan(PacMan pac)//Checks if this ghoul and PacMan share the same location
        {
            bool sameLocation = false;

            if ((pac.GetNextLocation(pac.CurrentDirection) == Location && GetNextLocation(CurrentDirection) == pac.Location) || Location == pac.Location)
            {
                sameLocation = true;
            }

            return sameLocation;
        }
    }
}
