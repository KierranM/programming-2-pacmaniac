﻿/**************************Class Description***************************
 * Class Name: Maze                                                   *
 *                                                                    *
 * Description: This class defines the maze that PacMan               *
 *              travels through. It is responsible for setting up the *
 *              DataGridView that is used to represent the maze and   *
 *              allowing it to be drawn to the form.                  *
 *                                                                    *
 **********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace PacManiac
{
    public class Maze :DataGridView
    {
        //Constants
        private const int GRIDSIZE = 20; //The number of rows and columns
        private const int CELLSIZE = 27; //Height and width of each square in the maze (in pixels)
        private const int PADDING = 4; //padding for the maze border
        public const string MAZE = @"Resources/Data/maze.dat";

        //Fields
        private Bitmap kibble;
        private Bitmap wall;
        private Bitmap blank;
        private string[,] gameMaze = new string[GRIDSIZE, GRIDSIZE];
        private string initialString;

        public Bitmap Kibble
        {
            get { return kibble; }
            set { kibble = value; }
        }

        public Bitmap Wall
        {
            get { return wall; }
            set { wall = value; }
        }

        public Bitmap Blank
        {
            get { return blank; }
            set { blank = value; }
        }

        public string[,] GameMaze
        {
            get { return gameMaze; }
            set { gameMaze = value; }
        }

        public Maze(string kibble, string wall, string blank) //This is the constructor for the Maze class, it uses the base constructor for a DataGridView. It initialises the Maze’s fields.
        {
            //Initialise the fields
            try
            {
                this.kibble = new Bitmap(kibble);
                this.wall = new Bitmap(wall);
                this.blank = new Bitmap(blank);
            }
            catch
            {
                MessageBox.Show("A fatal error has occured. Please reinstall the game", "Fatal Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }

            this.initialString = "";

            LoadMazeFromFile(); //Loads the maze data from the file
            ConvertStringTo2DArray(); //Converts the maze data from a 1D string into a 2D string array
            SetMazePosition(); //Set position of maze on gameForm
            SetUpColumnsAndRows(); //Set up the columns and rows
            SetMazeProperties(); //Sets the appropriate properties of the maze
            SetCellSize(); //Sets the height and width of each cell
            DisableCellResizing(); //Disables auto and user cell resizing
        }

        public void Draw() //This draws the maze on the form by looping through the gameMaze array and placing the correct image in the DataGridViewCell based on the character at the current position of the array.
        {
            for (int y = 0; y < GRIDSIZE; y++)
            {
                for (int x = 0; x < GRIDSIZE; x++)
                {
                    //Checking the character in the current X and Y position and sets the cell to the appropriate bitmap image
                    switch (gameMaze[y,x])
                    {
                        case "w":
                            this.Rows[y].Cells[x].Value = wall;
                            break;
                        case "k":
                            this.Rows[y].Cells[x].Value = kibble;
                            break;
                        case "b":
                            this.Rows[y].Cells[x].Value = blank;
                            break;
                        default:
                            MessageBox.Show("Unidentified value in string");
                            break;
                    }
                }
            }
        }

        public int CountKibbles()//This method loops through the gameMaze and counts the number of characters that match the kibble character “k”. It then returns this number as an int.
        {
            int noOfKibbles = 0;

            for (int y = 0; y < GRIDSIZE; y++)
            {
                for (int x = 0; x < GRIDSIZE; x++)
                {
                    if (this.Rows[y].Cells[x].Value == kibble)
                    {
                        noOfKibbles++;
                    }
                }
            }

            return noOfKibbles;
        }

        private void LoadMazeFromFile() //This method reads through the maze.dat file and places the resulting string into initialString.
        {
            StreamReader file = null;

            try
            {
                file = new StreamReader(MAZE);
            }
            catch
            {
                MessageBox.Show("A fatal error has occured. Please reinstall the game", "Fatal Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }

            initialString = file.ReadToEnd();

            file.Close();
        }

        private void ConvertStringTo2DArray() //This method takes the string in initialString and converts it into a 2D string array by looping through each character in initialString and placing it into the equivalent position in gameMaze.
        {
            for (int y = 0; y < GRIDSIZE; y++)
            {
                for (int x = 0; x < GRIDSIZE; x++)
                {
                    int position = (y * GRIDSIZE) + x;
                    gameMaze[y, x] = initialString.Substring(position, 1);
                }
            }
        }

        private void SetMazePosition()//This sets the position of the maze on the form by setting its Top and Left properties. They are set to 0 to place the maze in the top left corner of the form.
        {
            this.Top = 0;
            this.Left = 0;
        }

        private void SetUpColumnsAndRows()//This method creates DataGridViewImageColumns up to amount GRIDSIZE. It then adds rows up to amount GRIDSIZE. The result is a grid of GRIDSIZE * GRIDSIZE cells.
        {
            for (int x = 0; x < GRIDSIZE; x++)
            {
                Columns.Add(new DataGridViewImageColumn());
            }

            this.RowCount = GRIDSIZE;
        }

        private void SetMazeProperties() //This sets all the necessary DataGridView properties to their appropriate values
        {
            this.Height = GRIDSIZE * CELLSIZE + PADDING;
            this.Width = GRIDSIZE * CELLSIZE + PADDING;
            this.ScrollBars = ScrollBars.None;
            this.ColumnHeadersVisible = false;
            this.RowHeadersVisible = false;
            this.CellBorderStyle = DataGridViewCellBorderStyle.None;
            this.BorderStyle = BorderStyle.None;
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
        }

        private void SetCellSize() //This makes all the columns CELLSIZE wide and all the rows CELLSIZE high by looping through each column and then each row. The result is every cell being size CELLSIZE * CELLSIZE.
        {
            foreach (DataGridViewRow row in this.Rows)
            {
                row.Height = CELLSIZE;
            }
            foreach (DataGridViewColumn column in this.Columns)
            {
                column.Width = CELLSIZE;
            }
        }

        private void DisableCellResizing() //This disables the resizing of cells by the Maze or the user.
        {
            this.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;
            this.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;

            this.AllowUserToResizeRows = false;
            this.AllowUserToResizeColumns = false;
        }
    }
}
