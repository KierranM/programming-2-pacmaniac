﻿/**************************Class Description***************************
 * Class Name: Creature                                               *
 *                                                                    *
 * Description: This abstract class defines the base attributes and   *
 *              methods for all creatures involved in PacManiac.      *
 *              The class is abstract as any creatures in PacMan are  *
 *              supposed to have their own Creature derived class.    *
 *                                                                    *
 **********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace PacManiac
{
    public abstract class Creature
    {
        //Constants
        const string IMAGEPATH = @"Resources\Images\";
        const string BMPIMAGE = @".bmp";

        const int TUNNELPOSY = 9;
        const int LEFTTUNNELX = 0;
        const int RIGHTTUNNELX = 19;

        //Fields
        private string name;
        private Bitmap image;
        private Point location;
        private Direction currentDirection;
        private Maze maze;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public Bitmap Image
        {
            get { return image; }
            set { image = value; }
        }

        public Point Location
        {
            get { return location; }
            set { location = value; }
        }

        public Direction CurrentDirection
        {
            get { return currentDirection; }
            set { currentDirection = value; }
        }

        public Maze Maze
        {
            get { return maze; }
            set { maze = value; }
        }

        public Creature(string name, Point startingLocation, Maze maze) //This is the constructor for the creature class. As creature is abstract it is never intended to be used directly but is called from derived class constructors. It initialises the creatures fields.
        {
            this.name = name;
            this.location = startingLocation;
            this.maze = maze;

            try
            {
                this.image = new Bitmap(IMAGEPATH + name + BMPIMAGE);
            }
            catch
            {
                MessageBox.Show("A fatal error has occured. Please reinstall the game", "Fatal Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }
        }

        public void Move()//This changes the creatures location to the next location using the GetNextLocation method.
        {
            location = GetNextLocation(currentDirection);
        }

        public bool CheckCanMove(Direction directionToCheck) //This checks if the next cell in the creatures current direction is a valid cell to move into i.e not a wall cell.
        {
            bool canMove = false;
            Point nextLocation;

            nextLocation = GetNextLocation(currentDirection);

            DataGridViewCell cell;

            cell = GetCellFromPosition(nextLocation);

            if (cell.Value != maze.Wall)
            {
                canMove = true;
            }

            return canMove;
        }

        public Point GetNextLocation(Direction directionOfNextLocation) //This returns a Point equal to the next position in the given Direction.
        {
            Point nextLocation = location;

            switch (directionOfNextLocation)
            {
                case Direction.Down:
                    nextLocation.Y++;
                    break;
                case Direction.Right:
                    nextLocation.X++;
                    break;
                case Direction.Up:
                    nextLocation.Y--;
                    break;
                case Direction.Left:
                    nextLocation.X--;
                    break;
                default:
                    break;
            }

            if (nextLocation.Y == TUNNELPOSY && nextLocation.X < LEFTTUNNELX)
            {
                nextLocation.X = RIGHTTUNNELX;
            }
            if (nextLocation.Y == TUNNELPOSY && nextLocation.X > RIGHTTUNNELX)
            {
                nextLocation.X = LEFTTUNNELX;
            }

            return nextLocation;
        }

        public DataGridViewCell GetCellFromPosition(Point p) //Takes a point and returns the DataGridViewCell in that position of the maze.
        {
            DataGridViewCell cell;

            cell = maze.Rows[p.Y].Cells[p.X];

            return cell;
        }

        public abstract void Draw(); //This abstract method is designed to be overridden by derived classes.
    }
}
