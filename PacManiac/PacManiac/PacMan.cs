﻿/**************************Class Description***************************
 * Class Name: PacMan                                                 *
 *                                                                    *
 * Description: This class is derived from Creature and represents    *
 *              the protagonist of the game. It defines PacMan and    *
 *              what he does and how he reacts to controls.           *
 *                                                                    *
 **********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Media;
using System.Drawing;
using System.Windows.Media;
using System.Windows.Forms;

namespace PacManiac
{
    public class PacMan :Creature
    {
        //Constants
        const int STARTINGLIVES = 3;

        //Fields
        private MediaPlayer wakka;
        private SoundPlayer death;
        private Bitmap closedMouth;
        private int lives;

        public MediaPlayer Wakka
        {
            get { return wakka; }
            set { wakka = value; }
        }

        public SoundPlayer Death
        {
            get { return death; }
            set { death = value; }
        }

        public Bitmap ClosedMouth
        {
            get { return closedMouth; }
            set { closedMouth = value; }
        }

        public int Lives
        {
            get { return lives; }
            set { lives = value; }
        }

        public PacMan(string name, Point startingLocation, Maze maze, string soundPath) //This is the constructor for PacMan. It calls the base class constructor to initialise its base Creature attributes. It also initialises all of PacMan’s own fields.
            : base(name, startingLocation, maze)
        {
            try
            {
                this.wakka = new MediaPlayer();
                this.wakka.Open(new Uri(soundPath + "wakka.wav", UriKind.Relative));
                this.death = new SoundPlayer(soundPath + "death.wav");
                this.closedMouth = new Bitmap(@"Resources\Images\" + name + "closed.bmp");
            }
            catch
            {
                MessageBox.Show("A fatal error has occured. Please reinstall the game", "Fatal Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }

            this.lives = STARTINGLIVES;
        }

        public RotateFlipType GetImageRotation() //This returns the RotateFlipType used to rotate the image to match its direction. This makes PacMan moving around the maze look much nicer.
        {
            RotateFlipType rotFlip;

            switch (CurrentDirection)
            {
                case Direction.Down:
                    rotFlip = RotateFlipType.Rotate90FlipNone;
                    break;
                case Direction.Right:
                    rotFlip = RotateFlipType.RotateNoneFlipNone;
                    break;
                case Direction.Up:
                    rotFlip = RotateFlipType.Rotate270FlipNone;
                    break;
                case Direction.Left:
                    rotFlip = RotateFlipType.RotateNoneFlipX;
                    break;
                default:
                    rotFlip = RotateFlipType.RotateNoneFlipNone;
                    break;
            }

            return rotFlip;
        }

        public override void Draw() //This overrides the base class’ abstract Draw method. It uses GetCellFromPosition() from the base class and assigns the cell that is returned the value of image.
        {
                RotateFlipType rotFlip = GetImageRotation();

                DataGridViewCell pacManCell = GetCellFromPosition(Location);

                Bitmap rotatedImage = new Bitmap(Image);

                rotatedImage.RotateFlip(rotFlip);

                pacManCell.Value = rotatedImage;
            
        }

        public bool EatKibble() //Checks if PacMans location in the maze is also the location of a kibble character (“k”) in the Maze.GameMaze array. If it is then the “k” is replaced with a blank space character “b” and returns the Boolean value true.
        {
            bool ateAKibble = false;

            if (Maze.GameMaze[Location.Y, Location.X] == "k")
            {
                wakka.Play();
                wakka.Position = TimeSpan.MinValue;
                Maze.GameMaze[Location.Y, Location.X] = "b";
                ateAKibble = true;
            }

            return ateAKibble;
        }

        public void CloseMouth() //Takes PacMan’s current location and changes the image to the value of closedMouth.
        {
                RotateFlipType rotFlip = GetImageRotation();

                DataGridViewCell pacManCell = GetCellFromPosition(Location);

                Bitmap rotatedImage = new Bitmap(closedMouth);

                rotatedImage.RotateFlip(rotFlip);

                pacManCell.Value = rotatedImage;
            
        }

        public bool Dies() //This decrements PacMan’s lives by one. If the value of lives is less than zero then it returns false. If there are still lives remaining then it returns true.
        {
            bool livesRemaining = false;

            death.PlaySync();
            lives--;

            if (lives >= 0)
            {
                livesRemaining = true;
            }

            return livesRemaining;
        }

        public Direction GetDirectionFromPointToPacMan(Point location)//Returns the direction towards pacman from a point. Gives up/down coordinate first if the Y values are not the same
        {
            Direction direction;

            if (location.Y != Location.Y)
            {
                if (location.Y < Location.Y)
                {
                    direction = Direction.Down;
                }
                else
                {
                    direction = Direction.Up;
                }
            }
            else
            {
                if (location.X < Location.X)
                {
                    direction = Direction.Right;
                }
                else
                {
                    direction = Direction.Left;
                }
            }

            return direction;
        }
    }
}
