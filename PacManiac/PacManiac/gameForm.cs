﻿/* -----------------------------------------------------------------------------------------
			                        PacManiac - A PacMan Recreation
 ------------------------------------------------------------------------------------------

 Program name: 		PacManiac
 Project file name: PacManiac.sln
 Author:		    Kierran McPherson
 Date:	            02/11/2012
 Language:		    C#
 Platform:		    Microsoft Visual Studio Ultimate 2010
 Purpose:		    The purpose of the game is to get a high score by eating the most kibble without losing all of Mr. Pacs lives. 
                    The target audience of this game is truly anyone, such a simple game can be played by anyone. It is designed to provide the user with a fun and challenging experience as they try to beat their own or others high score. 
 
 Description:		PacMan has been a popular game since its creation and is still enjoyed many years later. 
                    PacManiac is a grid-based tribute to the classic game. In the original PacMan you moved an animated version of pacman around a maze, 
                    the original PacMan did not use a grid and so the animation was smooth as pacman moved back and forward. 
                    It is a simple and yet fun and challenging game, which I think makes it so enjoyable. Almost anyone can play PacMan as the controls are easy to understand. 
                    I aim to capture this simplicity in PacManiac.
 
 Known Bugs:		
    Major Bugs:
                    None known as yet.
 
    Minor Bugs:
                    PacMan doesnt disappear when killed like he should.
 
 Additional Features:

 
 ------------------------------------------------------------------------------------------
			                            Code space
 ------------------------------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Media;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Forms;

namespace PacManiac
{
    public partial class gameForm : Form
    {
        public gameForm()
        {
            InitializeComponent();
        }
        //Constants
        const int SLEEP = 500;

        const string KIBBLE = @"Resources\Images\kibble.bmp";
        const string WALL = @"Resources\Images\wall.bmp";
        const string BLANK = @"Resources\Images\blank.bmp";
        const string SOUND = @"Resources\Sounds\";

        Point blinkyStart = new Point(6, 7);
        Point pinkyStart = new Point(12, 7);
        Point inkyStart = new Point(12, 12);
        Point clydeStart = new Point(6, 12);
        Point mrPacStart = new Point(10, 18);

        bool gameOver;
        int timer = 0;
        int score = 0;

        MediaPlayer intro = new MediaPlayer();
        SoundPlayer siren;
        SoundPlayer victory;

        Random random = new Random();

        Maze maze;
        PacMan mrPac;

        Ghoul[] ghouls = new Ghoul[4];

        private void Form1_Load(object sender, EventArgs e) //All the instances and variables required for the game are initialised in here.
        {
            gameOver = false;

            siren = new SoundPlayer(SOUND + "siren.wav");
            victory = new SoundPlayer(SOUND + "victory.wav");

            intro.Open(new System.Uri(SOUND + "intro.wav", UriKind.Relative));


            this.Focus();
            maze = new Maze(KIBBLE, WALL, BLANK);
            this.Controls.Add(maze);

            mrPac = new PacMan("Mr. Pac", mrPacStart, maze, SOUND);
            FillLifeBoxes();

            CreateGhouls();
            SetDirections();
            FillOtherGhoulsArray();

            Redraw();

            intro.Play();
            siren.LoadAsync();
            siren.PlayLooping();

            timer1.Enabled = true;
        }

        private void CreateGhouls() //Initialises all the ghouls
        {
            ghouls[0] = new Ghoul("Blinky", blinkyStart, maze);
            ghouls[1] = new Ghoul("Pinky", pinkyStart, maze);
            ghouls[2] = new Ghoul("Inky", inkyStart, maze);
            ghouls[3] = new Ghoul("Clyde", clydeStart, maze);
        }

        private void SetDirections()
        {
            foreach (Ghoul ghoul in ghouls)
            {
                ghoul.CurrentDirection = (Direction)random.Next(4);
            }

            mrPac.CurrentDirection = Direction.Right;
        }

        private void FillOtherGhoulsArray() //Fills each ghouls array of ghouls by sending it all ghouls but itself
        {
            ghouls[0].OtherGhouls[0] = ghouls[1];
            ghouls[0].OtherGhouls[1] = ghouls[2];
            ghouls[0].OtherGhouls[2] = ghouls[3];

            ghouls[1].OtherGhouls[0] = ghouls[0];
            ghouls[1].OtherGhouls[1] = ghouls[2];
            ghouls[1].OtherGhouls[2] = ghouls[3];

            ghouls[2].OtherGhouls[0] = ghouls[0];
            ghouls[2].OtherGhouls[1] = ghouls[1];
            ghouls[2].OtherGhouls[2] = ghouls[3];

            ghouls[3].OtherGhouls[0] = ghouls[0];
            ghouls[3].OtherGhouls[1] = ghouls[1];
            ghouls[3].OtherGhouls[2] = ghouls[2];
        }

        private void timer1_Tick(object sender, EventArgs e) //This contains the code that causes the game to behave like a game like moving the ghouls, moving pacman etc.
        {
            timer++;

            if (timer % 2 == 0)
            {
                if (CheckIfPacManCaught() == false)
                {
                    MovePacMan();
                    MoveGhouls();
                    Redraw();
                }
            }
            else
            {
                mrPac.CloseMouth();
            }

            if (maze.CountKibbles() == 0)
            {
                timer1.Enabled = false;
                label4.Text = "You Win!";
                victory.Play();
            }
        }

        private bool CheckIfPacManCaught() //Checks if pacman has been caught and runs the PacMan.Dies() method. If that returns true then the it calls Restart() otherwise gameover
        {
            bool pacManDied = false;

            if (GhoulPacManCheck() == true)
            {
                timer1.Enabled = false;
                siren.Stop();
                Redraw();
                bool livesRemaining = mrPac.Dies();

                System.Threading.Thread.Sleep(SLEEP);
                if (livesRemaining == true)
                {
                    Restart();
                }
                else
                {
                    gameOver = true;
                    label4.Text = "You Lose!";
                }
                pacManDied = true;
            }
            return pacManDied;
        }

        private void Restart() //Resets the positions of the ghouls, makes pacman visible, redraws everything and fills the life boxes.
        {
            mrPac.CurrentDirection = Direction.Right;
            ResetPositions();
            Redraw();
            FillLifeBoxes();
            System.Threading.Thread.Sleep(SLEEP);
            timer1.Enabled = true;
        }

        private void MovePacMan() //Moves pacman
        {
            if (mrPac.CheckCanMove(mrPac.CurrentDirection) == true)
            {
                mrPac.Move();
                if (mrPac.EatKibble() == true)
                {
                    score++;
                    label2.Text = score.ToString();
                }
            }
        }

        private void MoveGhouls() //This Checks if all the ghouls can move and moves them if they can.
        {
            foreach (Ghoul ghoul in ghouls)
            {
                ghoul.PathFinder(mrPac, random);

                if (ghoul.CheckCanMove(mrPac, ghoul.CurrentDirection) == true)
                {
                    ghoul.Move();
                }
            }

        }

        private bool GhoulPacManCheck()//Checks if pacman is in contact with any of the ghouls and returns a Boolean value.
        {
            bool pacManDies = false;

            foreach (Ghoul ghoul in ghouls)
            {
                if (ghoul.CheckForPacMan(mrPac) == true)
                {
                    pacManDies = true;
                }
            }

            return pacManDies;
        }

        private void ResetPositions()//Resets the positions of all Creatures to their starting locations
        {
            mrPac.Location = mrPacStart;

            ghouls[0].Location = blinkyStart;
            ghouls[1].Location = pinkyStart;
            ghouls[2].Location = inkyStart;
            ghouls[3].Location = clydeStart;
        }

        private void Redraw()//Redraws all Creatures and the maze
        {
            
            maze.Draw();
            mrPac.Draw();
            
            foreach (Ghoul ghoul in ghouls)
            {
                ghoul.Draw();
            }
        }

        private void FillLifeBoxes() //Fills the pictureBoxes at the bottom of the screen with Mr. Pacs image
        {
            if (mrPac.Lives > 2)
            {
                pictureBox3.Image = mrPac.Image;
            }
            else
            {
                pictureBox3.Visible = false;
            }
            if (mrPac.Lives > 1)
            {
                pictureBox2.Image = mrPac.Image;
            }
            else
            {
                pictureBox2.Visible = false;
            }
            if (mrPac.Lives > 0)
            {
                pictureBox1.Image = mrPac.Image;
            }
            else
            {
                pictureBox1.Visible = false;
            }
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e) //Takes key presses and interprets which direction to change to
        {

            switch (e.KeyData)
            {
                case Keys.Down:
                    mrPac.CurrentDirection = Direction.Down;
                    break;
                case Keys.Left:
                    mrPac.CurrentDirection = Direction.Left;
                    break;
                case Keys.Right:
                    mrPac.CurrentDirection = Direction.Right;
                    break;
                case Keys.Up:
                    mrPac.CurrentDirection = Direction.Up;
                    break;
            }
        }
    }
}
